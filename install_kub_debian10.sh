#!/bin/bash

## Install Kubernets on debian 10

echo 'Delete the swap'
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
swapoff -a

echo 'Enable modules'
modprobe overlay
modprobe br_netfilter

echo 'Add some settings to sysctl'
tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sysctl --system

echo 'install Docker'
echo "deb https://download.docker.com/linux/debian buster stable" >> /etc/apt/sources.list
curl -sS https://download.docker.com/linux/debian/gpg | apt-key add -
###### Add (--exec-opt native.cgroupdriver=systemd) at the end of line: ExecStart: => /lib/systemd/system/docker.service 
apt update -y 
apt install docker-ce -y 

systemctl daemon-reload
/etc/init.d/docker restart

echo 'Installation de Kubernetes'
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" >> /etc/apt/sources.list
curl -sS https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
apt update -y 
apt install kubelet kubeadm -y
apt install kubectl -y 


# On the cluster 
kubeadm init --pod-network-cidr=10.244.0.0/16

# Pour installer kubens et kubectx
git clone https://github.com/ahmetb/kubectx /opt/kubectx
ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
ln -s /opt/kubectx/kubens /usr/local/bin/kubens
